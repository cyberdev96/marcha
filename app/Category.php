<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name'
    ];
    
    public function Photo()
    {
      return $this->hasOne('App\CategoryPhoto');
    }
    
    public function ItemPhotos()
    {
      return $this->hasMany('App\Subcategory');
    }
}
