<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryPhoto extends Model
{
    protected $fillable = [
        'category_id', 'file_name', 'mime_type', 'size', 'path'
    ];
}
