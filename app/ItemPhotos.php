<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPhotos extends Model
{
    protected $fillable = [
        'file_name', 'mime_type', 'size', 'path', 'item_id'
    ];
    
    public function Item()
    {
      return $this->belongsTo('App\Item');
    }
}
