<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id', 'name', 'date_of_birth', 'gender', 'mobile_number'
    ];
    
    public function DisplayPicture()
    {
      return $this->hasOne('App\UserDisplayPicture');
    }
    
    public function Location()
    {
      return $this->hasOne('App\UserLocation');
    }
    
    public function Items()
    {
      return $this->hasMany('App\Item');
    }
    
    public function Wishes()
    {
      return $this->hasMany('App\Wish');
    }
    
    public function InboxMessages()
    {
      return $this->hasMany('App\ExchangeInbox');
    }
    
    public function OutboxMessages()
    {
      return $this->hasMany('App\ExchangeOutbox');
    }
   
}
