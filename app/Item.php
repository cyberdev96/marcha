<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'name', 'description', 'worth', 'payment_allowed', 'item_status', 'category_id', 'subcategory_id', 'user_id'
    ];
    
    public function Profile()
    {
      return $this->belongsTo('App\Profile');
    }
    
    public function Location()
    {
      return $this->hasOne('App\ItemLocation');
    }
    
    public function Category()
    {
      return $this->hasOne('App\Category');
    }
    
    public function Subcategory()
    {
      return $this->hasOne('App\Subcategory');
    }
    
    public function Photos()
    {
      return $this->hasMany('App\ItemPhotos');
    }
}
