<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDisplayPicture extends Model
{
    protected $fillable = [
        'user_id', 'file_name', 'mime_type', 'size', 'path'
    ];
}
