<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeOutbox extends Model
{
    protected $fillable = [
        'exchange_id', 'to_id', 'to_item_id'
    ];
    
    public function Profile()
    {
      return $this->belongsTo('App\Profile');
    }
}
