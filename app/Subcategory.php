<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = [
        'name', 'category_id'
    ];
    
    public function Photo()
    {
      return $this->hasOne('App\SubcategoryPhoto');
    }
    
    public function Category()
    {
      return $this->belongsTo('App\Category');
    }
}
