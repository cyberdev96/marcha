<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    protected $fillable = [
        'description', 'status'
    ];
    
    public function Outbox()
    {
      return $this->hasOne('App\ExchangeOutbox');
    }
    
    public function Inbox()
    {
      return $this->hasOne('App\ExchangeInbox');
    }
}
