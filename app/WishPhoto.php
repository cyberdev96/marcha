<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishPhoto extends Model
{
    protected $fillable = [
        'wish_id', 'file_name', 'mime_type', 'size', 'path'
    ];
}
