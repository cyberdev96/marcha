<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubcategoryPhoto extends Model
{
    protected $fillable = [
        'subcategory_id', 'file_name', 'mime_type', 'size', 'path'
    ];
}
