<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemLocation extends Model
{
    protected $fillable = [
        'item_id', 'local_address', 'city', 'country', 'longitude', 'latitude'
    ];
}
