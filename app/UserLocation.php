<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model
{
    protected $fillable = [
        'user_id', 'local_address', 'city', 'country', 'longitude', 'latitude'
    ];
}
