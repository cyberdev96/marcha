<?php


use Illuminate\Database\Seeder;
use App\Category;
use App\CategoryPhoto;
use App\Subcategory;
use App\SubcategoryPhoto;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat = Category::create(array(
            'name' => 'Mobiles',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Books',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Electronics & Appliances',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Cars',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Bikes',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Furniture',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Pets',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Sports & Hobbies',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Fashion',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Services',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Jobs',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

        $cat = Category::create(array(
            'name' => 'Property',
        ));

        $catphoto = CategoryPhoto::create(array(
            'category_id' => $cat->id,
            'file_name' => 'sample-placeholder.png',
            'mime_type' => 'image/png',
            'size' => '121423',
            'path' => '/images/sample-placeholder.png'
        ));

    }
}
