<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserLocation;
use App\UserDisplayPicture;
use App\Profile;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        DB::table('users')->delete();
        DB::table('user_display_pictures')->delete();
        DB::table('user_locations')->delete();
        
        DB::table('profiles')->delete();
        
        DB::table('items')->delete();
        DB::table('item_photos')->delete();
        DB::table('item_locations')->delete();
        
        DB::table('categories')->delete();
        DB::table('category_photos')->delete();
        
        DB::table('subcategories')->delete();
        DB::table('subcategory_photos')->delete();
        
        DB::table('wishes')->delete();
        DB::table('wish_photos')->delete();
        
        DB::table('exchanges')->delete();
        DB::table('exchange_inboxes')->delete();
        DB::table('exchange_outboxes')->delete();

        $this->call('CategorySeeder');
        
        $user1 = User::create(array(
            'email' => '14bscssabbas@seecs.edu.pk',
            'password' => bcrypt('cd@12345'),
        ));
        
        $profile1 = Profile::create(array(
            'user_id' => $user1->id,
            'name' => 'Sayyed Shozib Abbas',
            'date_of_birth' => '1996-07-26',
            'gender' => 'Male',
            'mobile_number' => '03009849845',
        ));
        
        $address1 = UserLocation::create(array(
            'user_id' => $user1->id,
            'local_address' => 'House 123 Street 1 Sector A ',
            'city' => 'Islamabad',
            'country' => 'Pakistan',
        ));
        
        $display1 = UserDisplayPicture::create(array(
            'user_id' => $user1->id,
            'file_name' => 'placeholder.png',
            'mime_type' => 'image/png',
            'size' => '36895',
            'path' => '/images/placeholder.png'
        ));
    }
}
