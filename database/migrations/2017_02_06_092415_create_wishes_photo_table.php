<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWishesPhotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wish_photos', function (Blueprint $table) {
            $table->integer('wishes_id')->unsigned()->nullable();
            $table->foreign('wishes_id')->references('id')->on('wishes');
            $table->string('file_name');
            $table->string('mime_type');
            $table->string('size');
            $table->string('path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wish_photos');
    }
}
